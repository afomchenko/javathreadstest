package ru.anton;

import java.io.*;
import java.util.Queue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;


public class MyWriterThread implements Runnable {
    public Thread thread;
    private String filename;
    private BlockingQueue<String> buffer;


    public MyWriterThread(String filename, BlockingQueue<String> buffer) {
        thread = new Thread(this);
        this.buffer = buffer;
        this.filename = filename;
    }

    @Override
    public void run() {
        FileWriter fw = null;
        try {
            System.out.println("Запись начата: " + buffer.getClass().getSimpleName());

            //открываем потк записи
            fw = new FileWriter(filename);
            String line;

            while (true) {

                //проверяем есть ли что прочитать
                //если превышен таймер выходим
                if ((line = buffer.poll(100, TimeUnit.MILLISECONDS)) == null) {
                    System.out.println("Запись завершена: " + buffer.getClass().getSimpleName());
                    break;
                }

                //обработка текста и запись в выходной файл
                line = line.replaceAll("\\p{Punct}", "").toLowerCase().concat("\r\n");
                fw.write(line);
                fw.flush();
            }


        } catch (IOException e) {
            System.out.println(e);
        } catch (InterruptedException e) {
            return;
        } finally {
            if (fw != null)
                try {
                    fw.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
        }
    }
}