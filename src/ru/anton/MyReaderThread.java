package ru.anton;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Queue;
import java.util.concurrent.BlockingQueue;


public class MyReaderThread implements Runnable {
    public Thread thread;
    private String filename;
    private BlockingQueue<String> buffer;

    public MyReaderThread(String filename, BlockingQueue<String> buffer) {
        thread = new Thread(this);
        this.buffer = buffer;
        this.filename = filename;
    }

    @Override
    public void run() {
        FileReader fr = null;
        try {
            System.out.println("Чтение начато: " + buffer.getClass().getSimpleName());

            //открываем поток чтения
            fr = new FileReader(filename);
            BufferedReader bufReader = new BufferedReader(fr);
            String line;


            while ((line = bufReader.readLine()) != null) {
                try {
                    buffer.put(line);
                } catch (InterruptedException e) {
                    break;
                }
            }

            System.out.println("Чтение завершено: " + buffer.getClass().getSimpleName());


        } catch (FileNotFoundException e) {
            System.out.println(e);
        } catch (IOException e) {
            System.out.println(e);
        } finally {
            if (fr != null)
                try {
                    fr.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
        }
    }
}
