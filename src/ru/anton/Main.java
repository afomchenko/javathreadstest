package ru.anton;


//читаем из одного файла, удаляем знаки препинания и переводим в нижний регистр
//читаем и пишем в разных потоках

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class Main {

    public static void main(String[] args) {

        String in = "D:\\in.txt";
        String out = "D:\\out.txt";
        String out2 = "D:\\out2.txt";

        //своя реализация с MyQueue
        MyQueue<String> queue = new MyQueue<>(10);
        MyReaderThread reader= new MyReaderThread(in, queue);
        MyWriterThread writer= new MyWriterThread(out, queue);
        reader.thread.start();
        writer.thread.start();

        //с готовой блокирующейся очередью
        BlockingQueue<String> concQueue = new ArrayBlockingQueue<String>(100);
        MyReaderThread reader2= new MyReaderThread(in, concQueue);
        MyWriterThread writer2= new MyWriterThread(out2, concQueue);
        reader2.thread.start();
        writer2.thread.start();
    }
}
