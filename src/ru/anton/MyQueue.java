package ru.anton;

import java.util.*;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;


public class MyQueue<T> implements BlockingQueue<T> {

    private List<T> list;
    private final ReentrantLock lock = new ReentrantLock();
    Condition condition = lock.newCondition();
    private final int CAPACITY;

    public MyQueue(int size) {
        CAPACITY = size;
        list = new LinkedList<T>();
    }


    @Override
    public boolean add(T t) {
        lock.lock();
        try {
            if (list.size() >= CAPACITY) {
                throw new IllegalStateException("Queue full");
            }
            return list.add(t);
        } finally {
            condition.signalAll();
            lock.unlock();
        }
    }

    @Override
    public boolean offer(T t) {
        lock.lock();
        try {
            if (list.size() >= CAPACITY) {
                return false;
            }
            return list.add(t);
        } finally {
            condition.signalAll();
            lock.unlock();
        }
    }

    @Override
    public T remove() {
        lock.lock();
        try {
            if (!list.isEmpty()) {
                T item = list.get(0);
                list.remove(0);
                return item;
            } else throw new NoSuchElementException();
        } finally {
            condition.signalAll();
            lock.unlock();
        }
    }

    @Override
    public T poll() {
        lock.lock();
        try {
            if (!list.isEmpty()) {
                T item = list.get(0);
                list.remove(0);
                return item;
            } else return null;
        } finally {
            condition.signalAll();
            lock.unlock();
        }
    }

    @Override
    public T element() {
        lock.lock();
        try {
            if (!list.isEmpty()) {
                T item = list.get(0);
                return item;
            } else throw new NoSuchElementException();
        } finally {
            condition.signalAll();
            lock.unlock();
        }
    }

    @Override
    public T peek() {
        lock.lock();
        try {
            if (!list.isEmpty()) {
                T item = list.get(0);
                return item;
            } else return null;
        } finally {
            condition.signalAll();
            lock.unlock();
        }
    }

    @Override
    public void put(T t) throws InterruptedException {
        lock.lock();
        try {
            if (list.size() >= CAPACITY) {
                condition.await();
            }
            list.add(t);
            condition.signalAll();
        } finally {
            condition.signalAll();
            lock.unlock();
        }
    }

    @Override
    public boolean offer(T t, long timeout, TimeUnit unit) throws InterruptedException {
        lock.lock();
        try {
            if (list.size() >= CAPACITY) {
                condition.await(timeout, unit);
            }
            if (list.size() >= CAPACITY) {
                return false;
            } else {
                return list.add(t);
            }
        } finally {
            condition.signalAll();
            lock.unlock();
        }
    }

    @Override
    public T take() throws InterruptedException {
        lock.lock();
        try {
            while (list.isEmpty()) {
                condition.await();
            }
            T item = list.get(0);
            list.remove(0);
            return item;
        } finally {
            condition.signalAll();
            lock.unlock();
        }
    }

    @Override
    public T poll(long timeout, TimeUnit unit) throws InterruptedException {
        lock.lock();
        try {
            if (list.isEmpty()) {
                condition.await(timeout, unit);
            }
            if (list.isEmpty()) {
                return null;
            } else {
                T item = list.get(0);
                list.remove(0);
                return item;
            }
        } finally {
            condition.signalAll();
            lock.unlock();
        }
    }

    @Override
    public int remainingCapacity() {
        lock.lock();
        try {
            return CAPACITY - list.size();
        } finally {
            condition.signalAll();
            lock.unlock();
        }
    }

    @Override
    public boolean remove(Object o) {
        lock.lock();
        try {
            return list.remove(o);
        } finally {
            condition.signalAll();
            lock.unlock();
        }
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        lock.lock();
        try {
            return list.containsAll(c);
        } finally {
            condition.signalAll();
            lock.unlock();
        }
    }

    @Override
    public boolean addAll(Collection<? extends T> c) {
        lock.lock();
        try {
            return list.addAll(c);
        } finally {
            condition.signalAll();
            lock.unlock();
        }
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        lock.lock();
        try {
            return list.removeAll(c);
        } finally {
            condition.signalAll();
            lock.unlock();
        }
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        lock.lock();
        try {
            return list.retainAll(c);
        } finally {
            condition.signalAll();
            lock.unlock();
        }
    }

    @Override
    public void clear() {
        lock.lock();
        try {
            list.clear();
        } finally {
            condition.signalAll();
            lock.unlock();
        }
    }

    @Override
    public int size() {
        lock.lock();
        try {
            return list.size();
        } finally {
            condition.signalAll();
            lock.unlock();
        }
    }

    @Override
    public boolean isEmpty() {
        lock.lock();
        try {
            return list.isEmpty();
        } finally {
            condition.signalAll();
            lock.unlock();
        }
    }

    @Override
    public boolean contains(Object o) {
        lock.lock();
        try {
            return list.contains(o);
        } finally {
            condition.signalAll();
            lock.unlock();
        }
    }

    @Override
    public Iterator<T> iterator() {
        lock.lock();
        try {
            return list.iterator();
        } finally {
            condition.signalAll();
            lock.unlock();
        }
    }

    @Override
    public Object[] toArray() {
        lock.lock();
        try {
            return list.toArray();
        } finally {
            condition.signalAll();
            lock.unlock();
        }
    }

    @Override
    public <T1> T1[] toArray(T1[] a) {
        lock.lock();
        try {
            return list.toArray(a);
        } finally {
            condition.signalAll();
            lock.unlock();
        }
    }

    @Override
    public int drainTo(Collection<? super T> c) {
        lock.lock();
        try {
            if (c.size() > CAPACITY)
                throw new IllegalArgumentException("too big collection");

            list.clear();

            int i = 0;
            for (Object o : c) {
                list.add((T) o);
                i++;
            }

            return i;
        } finally {
            condition.signalAll();
            lock.unlock();
        }
    }

    @Override
    public int drainTo(Collection<? super T> c, int maxElements) {
        lock.lock();
        try {
            if (c.size() > CAPACITY)
                throw new IllegalArgumentException("too big collection");

            list.clear();

            int i = 0;
            for (Object o : c) {
                list.add((T) o);
                i++;
                if (i > maxElements) break;
            }

            return i;
        } finally {
            condition.signalAll();
            lock.unlock();
        }
    }
}

